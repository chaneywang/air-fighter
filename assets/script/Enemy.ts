import { _decorator, Collider2D, Component, Contact2DType, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Enemy')
export class Enemy extends Component {
    private _onDead: () => void;
    private _target: any;

    protected onEnable(): void {
        let collider = this.node.getComponent(Collider2D);
        collider?.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
    }

    protected onDisable(): void {
        let collider = this.node.getComponent(Collider2D);
        collider?.off(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
    }

    start() {

    }

    update(deltaTime: number) {

    }

    private onBeginContact(selfCollider: Collider2D, otherCollider: Collider2D): void {
        let bulletGrounp = 1 << 1;
        if (otherCollider.group === bulletGrounp) { //子弹
            //TODO 播放爆炸动画 
            this._onDead?.call(this._target);
            otherCollider.node.destroy();
            this.node.destroy();
        }
    }

    onDeadCallback(onDead: () => void, target?: any) {
        this._onDead = onDead;
        this._target = target;
    }
}
