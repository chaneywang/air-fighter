import { _decorator, Component, Node } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Maps')
export class Maps extends Component {

    speed: number = 1;

    protected onEnable(): void {
        this.node.children[0].setPosition(0, 0);
        this.node.children[1].setPosition(0, 480);
    }

    start() {

    }

    update(deltaTime: number) {
        for (let i = 0; i < this.node.children.length; i++) {
            let map = this.node.children[i];
            map.setPosition(0, map.position.y - this.speed);

            if (map.position.y <= -480) {
                map.setPosition(0, 480);
            }
        }
    }
}
