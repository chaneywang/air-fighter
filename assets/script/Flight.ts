import { _decorator, Asset, Component, instantiate, Node, Prefab, resources, Vec2 } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Flight')
export class Flight extends Component {

    velocity: Vec2 = new Vec2(0, 0);

    start() {

    }

    update(deltaTime: number) {
        this.node.setPosition(
            this.node.position.x + this.velocity.x,
            this.node.position.y + this.velocity.y);
    }

}
