import { _decorator, Asset, Component, EventKeyboard, Input, input, instantiate, KeyCode, Label, Node, Prefab, randomRangeInt, resources, tween, Vec3 } from 'cc';
import { Flight } from './Flight';
import { Enemy } from './Enemy';
const { ccclass, property } = _decorator;

@ccclass('Game')
export class Game extends Component {
    @property(Node)
    flight: Node;

    @property(Node)
    enemies: Node;

    @property(Node)
    bullets: Node;

    @property(Node)
    score: Node;

    private _flight: Flight;

    private _prefabEnemy: Prefab;
    private _prefabBullet: Prefab;

    private _score: number = 0;

    protected onLoad(): void {
        this._flight = this.flight.getComponent(Flight);
    }

    protected onEnable(): void {
        input.on(Input.EventType.KEY_DOWN, this.onKeyDown, this);
        input.on(Input.EventType.KEY_UP, this.onKeyUp, this);
    }

    start() {
        this._score = 0;
        this.createEnemy();
        this.startFlightShoot();
    }

    protected onDestroy(): void {
        this.unscheduleAllCallbacks();
    }

    update(deltaTime: number) {
        if (this.enemies.children.length < 2) {
            this.createEnemy();
        }
    }

    protected onDisable(): void {
        input.off(Input.EventType.KEY_DOWN, this.onKeyDown, this);
        input.off(Input.EventType.KEY_UP, this.onKeyUp, this);
    }

    private onKeyDown(event: EventKeyboard) {
        switch (event.keyCode) {
            case KeyCode.KEY_A:
                this._flight.velocity.set(-3, 0);
                break;
            case KeyCode.KEY_D:
                this._flight.velocity.set(3, 0);
                break;
            case KeyCode.KEY_W:
                this._flight.velocity.set(0, 3);
                break;
            case KeyCode.KEY_S:
                this._flight.velocity.set(0, -3);
                break;
            default:
                break;
        }
    }

    private onKeyUp(event: EventKeyboard) {
        switch (event.keyCode) {
            case KeyCode.KEY_A:
                this._flight.velocity.set(0, 0);
                break;
            case KeyCode.KEY_D:
                this._flight.velocity.set(0, 0);
                break;
            case KeyCode.KEY_W:
                this._flight.velocity.set(0, 0);
                break;
            case KeyCode.KEY_S:
                this._flight.velocity.set(0, 0);
                break;
            default:
                break;
        }
    }

    private onEmemyDead() {
        this._score++;
        this.score.getComponent(Label).string = `${this._score}`;
    }

    private createEnemy() {
        let createOne = () => {
            let enemyNode = instantiate(this._prefabEnemy);
            this.enemies.addChild(enemyNode);

            enemyNode.getComponent(Enemy).onDeadCallback(this.onEmemyDead, this);

            enemyNode.setPosition(-130, 120);
            tween(enemyNode)
                .to(3, { position: new Vec3(randomRangeInt(-60, 60), 120) }, { easing: 'expoOut' })
                .start();
        }

        if (!this._prefabEnemy) {
            resources.load('Enemy01', (err: Error, data: Asset) => {
                if (err) {
                    console.log(err);
                    return;
                }

                this._prefabEnemy = data as Prefab;
                createOne();
            });
        } else {
            createOne();
        }
    }

    private startFlightShoot() {
        let createBullet = () => {
            let bulletNode = instantiate(this._prefabBullet);
            this.bullets.addChild(bulletNode);
            bulletNode.setWorldPosition(this.flight.worldPosition);
        }

        this.schedule(() => {
            if (!this._prefabBullet) {
                resources.load('Bullet', (err: Error, data: Asset) => {
                    if (err) {
                        console.log(err);
                        return;
                    }
                    this._prefabBullet = data as Prefab;
                    createBullet();
                });
            } else {
                createBullet();
            }
        }, 0.5);
    }
}
